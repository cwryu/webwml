#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été signalé que phpMyAdmin, un outil web d'administration pour
MySQL, avait plusieurs vulnérabilités.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6606">CVE-2016-6606</a>

<p>Une paire de vulnérabilités a été découverte affectant la manière dont
les cookies sont stockés.</p>

<p>Le déchiffrement du nom d'utilisateur et mot de passe est vulnérable à
une attaque d'oracle par remplissage. Cela peut permettre à un attaquant
qui a accès au fichier de cookies du navigateur de l'utilisateur de
déchiffrer le nom d'utilisateur et le mot de passe.</p>

<p>Une vulnérabilité a été découverte où le même vecteur d'initialisation
est utilisé pour chiffrer le nom d'utilisateur et le mot de passe stockés
dans le cookie de phpMyAdmin. Si un utilisateur a un mot de passe identique
au nom d'utilisateur, un attaquant qui examine le cookie du navigateur peut
voir qu'ils sont identiques — mais l'attaquant ne peut pas déchiffrer
directement ces valeurs à partir du cookie parce qu'elles sont encore
chiffrées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6607">CVE-2016-6607</a>

<p>Une vulnérabilité de script intersite dans la fonctionnalité de
réplication.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6609">CVE-2016-6609</a>

<p>Un nom de base de données contrefait pour l'occasion pourrait être
utilisé pour exécuter des commandes PHP arbitraires au moyen de la
fonctionnalité d'export de tableau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6611">CVE-2016-6611</a>

<p>Un nom de base de données ou de table contrefait pour l'occasion peut
être utilisé pour déclencher une attaque d'injection SQL au moyen de la
fonctionnalité d'export de SQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6612">CVE-2016-6612</a>

<p>Un utilisateur peut exploiter la fonctionnalité LOAD LOCAL INFILE pour
exposer des fichiers présents sur le serveur au système de base de données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6613">CVE-2016-6613</a>

<p>Un utilisateur peut contrefaire pour l'occasion un lien symbolique sur
le disque, vers un fichier que phpMyAdmin a la permission de lire mais pas
l'utilisateur et que phpMyAdmin exposera à l'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6614">CVE-2016-6614</a>

<p>Une vulnérabilité a été signalée avec la fonctionnalité de remplacement
de nom d'utilisateur %u des fonctions SaveDir et UploadDir. Quand la
substitution de nom d'utilisateur est configurée, un nom d'utilisateur
contrefait pour l'occasion peut être utilisé pour contourner les
restrictions pour traverser le système de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6620">CVE-2016-6620</a>

<p>Une vulnérabilité a été signalée où certaines données sont passées à la
fonction unserialize() de PHP sans vérifier que les données sérialisées
sont valables. Du fait de la manière dont la fonction PHP opère, la
désérialisation peut avoir pour conséquence le chargement du code et son
exécution du fait de l'instanciation de l'objet et son chargement
automatique, et un utilisateur malveillant peut être capable d'exploiter
cela. Donc, un utilisateur malveillant peut être capable de manipuler les
données stockées de façon à exploiter cette faiblesse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6622">CVE-2016-6622</a>

<p>Un utilisateur non authentifié est capable d'exécuter une attaque par
déni de service en forçant des connexions persistantes quand phpMyAdmin est
exécuté avec l'option $cfg['AllowArbitraryServer']=true;.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6623">CVE-2016-6623</a>

<p>Un utilisateur autorisé malveillant peut provoquer une attaque par déni
de service sur un serveur en passant de grandes valeurs à une boucle.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6624">CVE-2016-6624</a>

<p>Une vulnérabilité a été découverte où, dans certaines circonstances, il
peut être possible de contourner les règles d'authentification basées sur
l'IP de phpMyAdmin. Quand phpMyAdmin est utilisé avec IPv6 dans un
environnement de serveur mandataire et que le serveur mandataire est dans
la plage d'adresse autorisée mais que l'ordinateur attaquant n'est pas
autorisé, cette vulnérabilité peut permettre à l'ordinateur attaquant
de se connecter en dépit des règles d'IP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6630">CVE-2016-6630</a>

<p>Un utilisateur authentifié peut déclencher une attaque par déni de
service en fournissant un mot de passe très long au dialogue de
modification de mot de passe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6631">CVE-2016-6631</a>

<p>Une vulnérabilité a été découverte où un utilisateur peut exécuter une
attaque d'exécution de code à distance à l'encontre d'un serveur quand
phpMyAdmin est exécuté en tant qu'application CGI. Avec certaines
configurations du serveur, une utilisation peut passer une chaîne de
requête qui est exécutée comme un argument en ligne de commande par des
scripts de l'interpréteur de commandes.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.4.11.1-2+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpmyadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-626.data"
# $Id: $
