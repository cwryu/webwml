#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La DLA 546-1 a été publiée de façon incorrecte avant que la mise à jour
des paquets de clamav ne soit disponible et il s'ensuit des problèmes avec
l'acceptation du paquet (qui a été depuis corrigé). Des mises à jour sont
maintenant disponibles pour toutes les architectures prises en charge par
LTS.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>La version 0.99.2 a été publiée par l'amont. Cette actualisation met à
jour wheezy-lts vers la dernière version en ligne conforme à l'approche
adoptée pour les autres versions de Debian.</p>

<p>Ces modifications ne sont pas strictement requises que clamav
fonctionne, mais les utilisateurs des versions précédentes dans Wheezy
peuvent ne pas pouvoir utiliser toutes les signatures de virus actuelles
et pourraient recevoir des avertissements.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.99.2+dfsg-0+deb7u2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 0.99.2+dfsg-0+deb7u2 de clamav.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-546-2.data"
# $Id: $
