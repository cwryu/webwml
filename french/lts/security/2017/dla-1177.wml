#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>poppler, une bibliothèque de rendu de PDF, était affectée par plusieurs
bogues de déni de service (plantage d'application), de déréférencement de
pointeur NULL et de lecture hors limites de tampon basé sur le tas.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14975">CVE-2017-14975</a>

<p>La fonction FoFiType1C::convertToType0 dans FoFiType1C.cc avait une
vulnérabilité de déréférencement de pointeur NULL à cause d’une structure de
données non initialisée. Cela permet à un attaquant de lancer une attaque par
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14976">CVE-2017-14976</a>

<p>La fonction FoFiType1C::convertToType0 dans FoFiType1C.cc a une vulnérabilité
de lecture hors limites de tampon basé sur le tas si un index hors limites de
dictionnaire de fontes est rencontré. Cela permet à un attaquant de lancer une
attaque par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14977">CVE-2017-14977</a>

<p>La fonction FoFiTrueType::getCFFBlock dans FoFiTrueType.cc a une
vulnérabilité de déréférencement de pointeur NULL à cause du manque de
validation d’un pointeur de table. Cela permet à un attaquant le lancer une
attaque par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15565">CVE-2017-15565</a>

<p>Un déréférencement de pointeur NULL existe dans la fonction
GfxImageColorMap::getGrayLine() dans GfxState.cc à l'aide d'un document PDF
contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.18.4-6+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1177.data"
# $Id: $
