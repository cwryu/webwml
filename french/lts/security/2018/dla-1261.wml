#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans clamav, la boîte à
outils antivirus ClamAV pour Unix. Leurs effets vont du déni de service à
une potentielle exécution de code arbitraire. En complément, cette version
corrige un problème ancien qui a récemment refait surface à cause duquel
une base de données de signatures de virus mal formées peut provoquer un
plantage de l'application et un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12374">CVE-2017-12374</a>

<p>ClamAV a une situation d'utilisation de mémoire après libération
découlant d'une absence de validation des entrées. Un attaquant distant
pourrait exploiter cette vulnérabilité avec un message électronique
contrefait pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12375">CVE-2017-12375</a>

<p>ClamAV a une vulnérabilité de dépassement de tampon découlant d'une
absence de validation des entrées. Un attaquant distant non authentifié
pourrait envoyer un message électronique contrefait au périphérique
affecté, déclenchant un dépassement de tampon et éventuellement un déni de
service lors de l'examen du message malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12376">CVE-2017-12376</a>

<p>ClamAV a une vulnérabilité de dépassement de tampon découlant d'une
mauvaise validation des entrées lors du traitement de fichiers Portable
Document Format (PDF). Un attaquant distant non authentifié pourrait
envoyer un fichier PDF contrefait au périphérique affecté, déclenchant un
dépassement de tampon et éventuellement un déni de service ou l'exécution
de code arbitraire lors de l'examen du fichier malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12377">CVE-2017-12377</a>

<p>ClamAV a une vulnérabilité de dépassement de tas découlant d'une
mauvaise validation des entrées lors du traitement de paquets mew. Un
attaquant pourrait l'exploiter en envoyant un message contrefait au
périphérique affecté, déclenchant un déni de service ou une possible
exécution de code arbitraire lors de l'examen du fichier malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12378">CVE-2017-12378</a>

<p>ClamAV a une vulnérabilité de lecture hors limite de tampon découlant
d'une mauvaise validation des entrées lors du traitement de fichiers
d'archive sur bande (TAR). Un attaquant distant non authentifié pourrait
envoyé un fichier TAR contrefait au périphérique affecté, déclenchant une
lecture hors limite de tampon et éventuellement un déni de service lors de
l'examen du fichier malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12379">CVE-2017-12379</a>

<p>ClamAV a une vulnérabilité de dépassement de tampon découlant d'une
mauvaise validation des entrées dans la fonction d'analyse des messages. Un
attaquant distant non authentifié pourrait envoyer un message électronique
contrefait au périphérique affecté, déclenchant un dépassement de tampon et
éventuellement un déni de service ou l'exécution de code arbitraire lors de
l'examen du fichier malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12380">CVE-2017-12380</a>

<p>ClamAV a une vulnérabilité de déréférencement de pointeur NULL découlant
d'une mauvaise validation des entrées dans la fonction d'analyse des
messages. Un attaquant distant non authentifié pourrait envoyer un message
électronique contrefait au périphérique affecté, déclenchant un
déréférencement de pointeur NULL qui peut avoir pour conséquence un déni de
service.</p>

<li>Debian Bug n°824196

<p>Une base de données de signatures de virus mal formées pourrait
provoquer un plantage de l'application et un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.99.2+dfsg-0+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1261.data"
# $Id: $
