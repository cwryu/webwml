#use wml::debian::translation-check translation="0ae0c2a40542003d1bcc83846a868c21594e3dec" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de dépassement d’entier dans librabbitmq, une
bibliothèque de passage de messages robuste entre applications et serveurs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18609">CVE-2019-18609</a>

<p>Un problème a été découvert dans amqp_handle_input dans amqp_connection.c
dans rabbitmq-c 0.9.0. Il existait un dépassement d'entier aboutissant à une
corruption de mémoire de tas dans le traitement de CONNECTION_STATE_HEADER. Un
serveur véreux pourrait renvoyer un en-tête de trame malveillant aboutissant à
une valeur de target_size plus petite que celle nécessaire. Cette condition est
alors reproduite vers une fonction memcpy qui copie trop de données dans un
tampon de tas.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.5.2-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets librabbitmq.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2022.data"
# $Id: $
