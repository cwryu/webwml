<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a XML external entity vulnerability
in the <em>lemonldap-ng</em> single-sign on system. This may have led to the
disclosure of confidential data, denial of service, server side
request forgery, port scanning, etc.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13031">CVE-2019-13031</a>

    <p>LemonLDAP::NG before 1.9.20 has an XML External Entity (XXE) issue when submitting a notification to the notification server. By default, the notification server is not enabled and has a "deny all" rule.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.3-1+deb8u2.</p>

<p>We recommend that you upgrade your lemonldap-ng packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1844.data"
# $Id: $
