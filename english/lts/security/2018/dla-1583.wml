<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in the JasPer
JPEG-2000 library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5203">CVE-2015-5203</a>

    <p>Gustavo Grieco discovered an integer overflow vulnerability that
    allows remote attackers to cause a denial of service or may have
    other unspecified impact via a crafted JPEG 2000 image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5221">CVE-2015-5221</a>

    <p>Josselin Feist found a double-free vulnerability that allows remote
    attackers to cause a denial-of-service (application crash) by
    processing a malformed image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8690">CVE-2016-8690</a>

    <p>Gustavo Grieco discovered a NULL pointer dereference vulnerability
    that can cause a denial-of-service via a crafted BMP image file. The
    update also includes the fixes for the related issues <a href="https://security-tracker.debian.org/tracker/CVE-2016-8884">CVE-2016-8884</a>
    and <a href="https://security-tracker.debian.org/tracker/CVE-2016-8885">CVE-2016-8885</a> which complete the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2016-8690">CVE-2016-8690</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13748">CVE-2017-13748</a>

    <p>It was discovered that jasper does not properly release memory used
    to store image tile data when image decoding fails which may lead to
    a denial-of-service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14132">CVE-2017-14132</a>

    <p>A heap-based buffer over-read was found related to the
    jas_image_ishomosamp function that could be triggered via a crafted
    image file and may cause a denial-of-service (application crash) or
    have other unspecified impact.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.900.1-debian1-2.4+deb8u4.</p>

<p>We recommend that you upgrade your jasper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1583.data"
# $Id: $
