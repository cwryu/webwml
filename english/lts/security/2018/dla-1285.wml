<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>BIND, a DNS server implementation, was found to be vulnerable to a denial
of service flaw was found in the handling of DNSSEC validation. A remote
attacker could use this flaw to make named exit unexpectedly with an
assertion failure via a specially crafted DNS response. This issue is
closely related to <a href="https://security-tracker.debian.org/tracker/CVE-2017-3139">CVE-2017-3139</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.8.4.dfsg.P1-6+nmu2+deb7u20.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1285.data"
# $Id: $
