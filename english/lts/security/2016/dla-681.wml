<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata up to 2016h. Notable
changes are:</p>

<ul>
 <li>Asia/Gaza and Asia/Hebron (DST ending on 2016-10-29 at 01:00,
   not 2016-10-21 at 00:00).</li>
 <li>Europe/Istanbul switch from EET/EEST (+02/+03) to permanent +03 on
   2016-09-07. While the timezone has changed, the divergence from
   EET/EEST will happen on 2016-10-30.</li>
 <li>Turkey switched from EET/EEST (+02/+03) to permanent +03,
   effective 2016-09-07.</li>
 <li>New leap second 2016-12-31 23:59:60 UTC as per IERS Bulletin C 52.</li>
</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2016h-0+deb7u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-681.data"
# $Id: $
