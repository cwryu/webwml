<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security issues have been discovered in the Squid caching proxy.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4051">CVE-2016-4051</a>

    <p>CESG and Yuriy M. Kaminskiy discovered that Squid cachemgr.cgi
    was vulnerable to a buffer overflow when processing remotely supplied
    inputs relayed through Squid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4052">CVE-2016-4052</a>


    <p>CESG discovered that a buffer overflow made Squid vulnerable to a
    Denial of Service (DoS) attack when processing ESI responses.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4053">CVE-2016-4053</a>

    <p>CESG found that Squid was vulnerable to public information disclosure
    of the server stack layout when processing ESI responses.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4054">CVE-2016-4054</a>

    <p>CESG discovered that Squid was vulnerable to remote code execution
    when processing ESI responses.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4554">CVE-2016-4554</a>

    <p>Jianjun Chen found that Squid was vulnerable to a header smuggling
    attack that could lead to cache poisoning and to bypass of same-origin
    security policy in Squid and some client browsers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4555">CVE-2016-4555</a>

<p>and <a href="https://security-tracker.debian.org/tracker/CVE-2016-4556">CVE-2016-4556</a></p>

    <p>"bfek-18" and "@vftable" found that Squid was vulnerable to a Denial
    of Service (DoS) attack when processing ESI responses, due to
    incorrect pointer handling and reference counting.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in squid3 version
3.1.20-2.2+deb7u5. We recommend you to upgrade your squid3 packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-478.data"
# $Id: $
