<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Christian Reitter discovered that libu2f-host, a library implementing
the host-side of the U2F protocol, failed to properly check for a
buffer overflow. This would allow an attacker with a custom made
malicious USB device masquerading as a security key, and physical
access to a computer where PAM U2F or an application with libu2f-host
integrated, to potentially execute arbitrary code on that computer.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 1.1.2-2+deb9u1.</p>

<p>We recommend that you upgrade your libu2f-host packages.</p>

<p>For the detailed security status of libu2f-host please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libu2f-host">\
https://security-tracker.debian.org/tracker/libu2f-host</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4389.data"
# $Id: $
