<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the webkit2gtk web
engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8644">CVE-2019-8644</a>

    <p>G. Geshev discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8649">CVE-2019-8649</a>

    <p>Sergei Glazunov discovered an issue that may lead to universal
    cross site scripting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8658">CVE-2019-8658</a>

    <p>akayn discovered an issue that may lead to universal cross site
    scripting.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8666">CVE-2019-8666</a>

    <p>Zongming Wang and Zhe Jin discovered memory corruption issues that
    can lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8669">CVE-2019-8669</a>

    <p>akayn discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8671">CVE-2019-8671</a>

    <p>Apple discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8672">CVE-2019-8672</a>

    <p>Samuel Gross discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8673">CVE-2019-8673</a>

    <p>Soyeon Park and Wen Xu discovered memory corruption issues that
    can lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8676">CVE-2019-8676</a>

    <p>Soyeon Park and Wen Xu discovered memory corruption issues that
    can lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8677">CVE-2019-8677</a>

    <p>Jihui Lu discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8678">CVE-2019-8678</a>

    <p>An anonymous researcher, Anthony Lai, Ken Wong, Jeonghoon Shin,
    Johnny Yu, Chris Chan, Phil Mok, Alan Ho, and Byron Wai discovered
    memory corruption issues that can lead to arbitrary code
    execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8679">CVE-2019-8679</a>

    <p>Jihui Lu discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8680">CVE-2019-8680</a>

    <p>Jihui Lu discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8681">CVE-2019-8681</a>

    <p>G. Geshev discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8683">CVE-2019-8683</a>

    <p>lokihardt discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8684">CVE-2019-8684</a>

    <p>lokihardt discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8686">CVE-2019-8686</a>

    <p>G. Geshev discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8687">CVE-2019-8687</a>

    <p>Apple discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8688">CVE-2019-8688</a>

    <p>Insu Yun discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8689">CVE-2019-8689</a>

    <p>lokihardt discovered memory corruption issues that can lead to
    arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8690">CVE-2019-8690</a>

    <p>Sergei Glazunov discovered an issue that may lead to universal
    cross site scripting.</p></li>

</ul>

<p>You can see more details on the WebKitGTK and WPE WebKit Security
Advisory WSA-2019-0004.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.24.4-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4515.data"
# $Id: $
