#use wml::debian::translation-check translation="4c0996e191dd68b6dfbadfee3c048714d4cfa961" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sårbarheder er opdaget i Xen-hypervisoren, hvilke kunne medføre 
lammelsesangreb, gæst til vært-rettighedsforøgelse eller 
informationslækager.</p>

<p>Desuden indeholder denne opdatering en afhjælpelse af det spekulative 
sidekanalangreb <q>TSX Asynchronous Abort</q>.  For flere oplysninger, se 
<a href="https://xenbits.xen.org/xsa/advisory-305.html">\
https://xenbits.xen.org/xsa/advisory-305.html</a>.</p>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 4.8.5.final+shim4.10.4-1+deb9u12.  Bemærk at dette er den sidste 
sikkerhedsopdatering af Xen i den gamle stabile distribution; 
opstrømsunderstøttelse af 4.8.x-forgreningen ophørte med udgangen af december 
2019.  Hvis du er afhængig af at din Xen-installation har 
sikkerhedsunderstøttelse anbefales det at opdatere til den stabile distribution 
(buster).</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 4.11.3+24-g14b62ab3e5-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine xen-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende xen, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4602.data"
