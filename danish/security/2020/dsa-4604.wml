#use wml::debian::translation-check translation="73b976c71b8b4c13c331a478bd9111aa6f64627e" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige problemer er fundet i cacti, et serverovervågningssystem, 
potentielt medførende udførelse af SQL eller informationsafsløring 
foretaget af autentificerede brugere.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16723">CVE-2019-16723</a>

    <p>Autentificerede brugere kunne omgå autorisationskontrollerne vil visning 
    af en graf, ved at indsende forespørgsler med modificerede 
    local_graph_id-parametre.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17357">CVE-2019-17357</a>

    <p>Brugerfladen til administration af graf fornuftighedskontrollerede på 
    utilstrækkelig vis paremeteret template_id, potentielt førende til 
    SQL-indsprøjtning.  Sårbarheden kunne anvendes som løftestang af 
    autentificerede angribere, til at udføre uautoriseret SQL-kode i en 
    database.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17358">CVE-2019-17358</a>

    <p>Funktionen sanitize_unserialize_selected_items (lib/functions.php) 
    fornuftighedskontrollerede på utilstrækkelig vis brugerinddata, før de blev 
    deserialiseret, potentielt medførende usikker deserialisering af 
    brugerkontrollerede data.  Sårbarheden kunne anvendes som løftestang af 
    autentificerede angribere til at påvirke programkontrolforløbet eller til at 
    forårsage hukommelseskorruption.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 0.8.8h+ds1-10+deb9u1.  Bemærk at stretch kun var påvirket af 
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17358">\
CVE-2018-17358</a>.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1.2.2+ds1-2+deb10u2.</p>

<p>Vi anbefaler at du opgraderer dine cacti-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende cacti, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/cacti">\
https://security-tracker.debian.org/tracker/cacti</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4604.data"
