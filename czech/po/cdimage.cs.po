# cdimage.cs.po
# Copyright (C) 2004 Free Software Foundation, Inc.
# Vilém Vychodil <vilem.vychodil@upol.cz>, 2003
# Juraj Kubelka <Juraj.Kubelka@email.cz>, 2004
# Michal Simunek <michal.simunek@gmail.com>, 2011
#
msgid ""
msgstr ""
"Project-Id-Version: cdimage 1.2\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2011-02-01 12:48+0100\n"
"Last-Translator: Michal Simunek <michal.simunek@gmail.com>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />Často kladené otázky"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Stáhnout pomocí Jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Stáhnout přes HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Zakoupení CD nebo DVD"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Síťová instalace"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Stažení"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Ostatní"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Grafika"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Zrcadlení"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Zrcadla Rsync"

#: ../../english/template/debian/cdimage.wml:43
#, fuzzy
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_misc\" />Ostatní"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Stáhnout pomocí Torrentu"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_relinfo\" />Image Release Info"
msgstr "<void id=\"dc_relinfo\" />Informace o vydání obrazu"

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Tým Debian CD"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr "debian_na_cd"

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />často_kladené_otázky"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr "http_ftp"

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "zakoupení"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr "síťová_instalace"

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />ostatní"

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"<a href=\"/MailingLists/disclaimer\">veřejná konference</a> v angličtině "
"věnující se CD a DVD:"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "      Fingerprint klíèe"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr ""

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr ""
