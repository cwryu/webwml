#use wml::debian::template title="Debian bullseye &mdash; руководство по установке" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="a4056e907e44ae14172feaedc8e3e0bb596aa260" maintainer="Lev Lamberov"

<if-stable-release release="stretch">
<p>Это <strong>бета версия</strong> руководства по установке Debian
10, кодовое имя buster, который пока не выпущен. Представленная здесь
информация может быть устаревшей и/или неверной из-за изменений
установщика. Вам, вероятно, требуется
<a href="../stretch/installmanual">Руководство по установке Debian
9, кодовое имя stretch</a>, который является последней выпущенной версией
Debian; или <a href="https://d-i.debian.org/manual/">Разрабатываемая
версия руководства по установке</a>, которая является наиболее актуальной версией
настоящего документа.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Это <strong>бета версия</strong> руководства по установке Debian
11, кодовое имя bullseye, который пока не выпущен. Представленная здесь
информация может быть устаревшей и/или неверной из-за изменений
установщика. Вам, вероятно, требуется
<a href="../buster/installmanual">Руководство по установке Debian
10, кодовое имя buster</a>, который является последней выпущенной версией
Debian; или <a href="https://d-i.debian.org/manual/">Разрабатываемая
версия руководства по установке</a>, которая является наиболее актуальной версией
настоящего документа.</p>
</if-stable-release>

<p>Руководства по установке, включая загружаемые файлы, доступны для
каждой из поддерживаемых архитектур:</p>

<ul>
<:= &permute_as_list('', 'Руководство по установке'); :>
</ul>

<p>Если в вашем браузере верно установлены параметры вашего языка,
то по приведённым выше ссылкам вы автоматически получите HTML-версию на
вашем языке&nbsp;&mdash; смотрите <a href="$(HOME)/intro/cn">
информацию о согласовании содержания</a>. В противном случае
выберите необходимые вам архитектуру, язык и формат из таблицы ниже.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Архитектура</strong></th>
  <th align="left"><strong>Формат</strong></th>
  <th align="left"><strong>Языки</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   $_[2] eq html ? "$_[0].$_[1].$_[2]" : "$_[0].$_[2].$_[1]" } ); :>
</table>
</div>
