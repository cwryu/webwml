#use wml::debian::translation-check translation="0cfee114b2d0c22d16e262ece0a0804aac60d237"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en el motor de JSP y servlets
Tomcat que podrían dar lugar a «contrabando» de peticiones HTTP («HTTP request smuggling»), a ejecución de código
en el contector AJP (inhabilitado por omisión en Debian) o a ataques por intermediario
(«man-in-the-middle») contra la interfaz JMX.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 9.0.31-1~deb10u1. La corrección para <a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">\
CVE-2020-1938</a> puede hacer necesarios
cambios de configuración cuando se utiliza Tomcat con el conector AJP, como puede ser
en combinación con libapache-mod-jk. A modo de ejemplo, la propiedad
<q>secretRequired</q> ahora toma el valor true por omisión. Para información sobre configuraciones afectadas, se
recomienda revisar <a href="https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html">\
https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html</a>
antes de desplegar la actualización.</p>

<p>Le recomendamos que actualice los paquetes de tomcat9.</p>

<p>Para información detallada sobre el estado de seguridad de tomcat9, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4680.data"
